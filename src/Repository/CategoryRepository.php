<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function getCategoriesChain($initialId): array
    {
        $categoriesAll = $this->findAll();
        $result = $this->getChain($categoriesAll, $initialId);
        return $result;
    }

    private function getChain($categoriesAll, int $initialId):array
    {
        $arr = [$initialId];
        foreach ($categoriesAll as $val) {
            if ($val->getCatParentCat() === $initialId) {
                $arr = array_merge($arr, $this->getChain($categoriesAll, $val->getId()));
            }
        }
        return $arr;
    }
    private function getChain1($categoriesAll, int $initialId):array
    {
        foreach ($categoriesAll as $val) {
            if ($val->getCatParentCat() === $initialId) {
            }
        }
    }


    public function getBreadCrumps($initialId)
    {
        $categoriesAll = $this->findAll();
        foreach ($categoriesAll as $cat) {
            if ($cat->getId() == $initialId) {
                $currentName = $cat->getCatName();
            }
        }
        $cahin = $this->getBreadCrumpsChain($categoriesAll, $initialId);
        return array_reverse($cahin);
    }

    private function getBreadCrumpsChain($categoriesAll, $initialId) {
        $arr = [['id'=> 0, 'name'=>'main']];
        foreach ($categoriesAll as $cat) {
            if ($cat->getId() == $initialId) {
                $arr = [['id' => $cat->getId(), 'name' => $cat->getCatName()]];
                $arr = array_merge($arr, $this->getBreadCrumpsChain($categoriesAll, $cat->getCatParentCat()));
            }
        }
        return $arr;
    }

/*
    public function getBreadCrumps($initialId)
    {
        $categoriesAll = $this->findAll();
        foreach ($categoriesAll as $cat) {
            if ($cat->getId == $initialId) {
                $currentName = $cat->getCatName();
            }
        }
        $cahin = $this->getBreadCrumpsChain($categoriesAll, $initialId, $currentName);
        return $cahin;
    }

    private function getBreadCrumpsChain($categoriesAll, $initialId, $currentName) {
        $arr = [$initialId => $currentName];
        if ($initialId == 0) {
            return $arr;
        }
        foreach ($categoriesAll as $cat) {
            if ($cat->getId() == $initialId) {
                $arr = array_merge($arr, $this->getBreadCrumpsChain($categoriesAll, $cat->getCatParentCat(), $cat->getCatName()));
            }
        }
        return $arr;
    }
*/
//    /**
//     * @return Category[] Returns an array of Category objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Category
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

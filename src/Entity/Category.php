<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $cat_name;

    /**
     * @ORM\Column(type="integer")
     */
    private $cat_parent_cat;

    /**
     * @ORM\Column(type="integer")
     */
    private $cat_name_id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Advertisements", mappedBy="ads_category")
     */
    private $advertisments;

    public function __construct()
    {
        $this->advertisments = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCatName(): ?string
    {
        return $this->cat_name;
    }

    public function setCatName(string $cat_name): self
    {
        $this->cat_name = $cat_name;

        return $this;
    }

    public function getCatParentCat(): ?int
    {
        return $this->cat_parent_cat;
    }

    public function setCatParentCat(int $cat_parent_cat): self
    {
        $this->cat_parent_cat = $cat_parent_cat;

        return $this;
    }

    public function getCatNameId(): ?int
    {
        return $this->cat_name_id;
    }

    public function setCatNameId(int $cat_name_id): self
    {
        $this->cat_name_id = $cat_name_id;

        return $this;
    }

    /**
     * @return Collection|Advertisements[]
     */
    public function getAdvertisments(): Collection
    {
        return $this->advertisments;
    }

    public function addAdvertisment(Advertisements $advertisment): self
    {
        if (!$this->advertisments->contains($advertisment)) {
            $this->advertisments[] = $advertisment;
            $advertisment->setAdsCategory($this);
        }

        return $this;
    }

    public function removeAdvertisment(Advertisements $advertisment): self
    {
        if ($this->advertisments->contains($advertisment)) {
            $this->advertisments->removeElement($advertisment);
            // set the owning side to null (unless already changed)
            if ($advertisment->getAdsCategory() === $this) {
                $advertisment->setAdsCategory(null);
            }
        }

        return $this;
    }
}

<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form;

use App\Entity\Category;
use App\Entity\Picture;
use App\Entity\Tag;
use App\Repository\CategoryRepository;
use App\Repository\TagRepository;
use App\Service\FileUploader;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * This data transformer is used to translate the array of tags into a comma separated format
 * that can be displayed and managed by Bootstrap-tagsinput js plugin (and back on submit).
 *
 * See https://symfony.com/doc/current/form/data_transformers.html
 *
 * @author Yonel Ceruto <yonelceruto@gmail.com>
 * @author Jonathan Boyer <contact@grafikart.fr>
 */
class PicturesTransformer implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function transform($int)
    {
        return $int;
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($pictures)
    {
        foreach ($pictures as $pic) {
            $picObj = new Picture();
            $picObj->file = $pic;
            $picturesObj[] = $picObj;
        }

        return $picturesObj;
    }
}

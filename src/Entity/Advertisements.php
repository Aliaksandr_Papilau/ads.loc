<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AdvertisementsRepository")
 */
class Advertisements
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ads_title;

    /**
     * @ORM\Column(type="text")
     */
    private $ads_description;

    /**
     * @ORM\Column(type="float")
     */
    private $ads_price;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $ads_email;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $ads_skype;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $ads_login;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $ads_password;

    /**
     * @ORM\Column(type="integer")
     */
    private $ads_category_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Locations", inversedBy="advertisemets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ads_location_id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $ads_phone;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency", inversedBy="adverticements")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ads_currency_id;


    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ads_views;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $ads_date;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Picture", mappedBy="pic_ads_id", cascade={"persist"})
     */
    private $Pictures;

    public function __construct()
    {
        $this->Pictures = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAdsTitle(): ?string
    {
        return $this->ads_title;
    }

    public function setAdsTitle(string $ads_title): self
    {
        $this->ads_title = $ads_title;

        return $this;
    }

    public function getAdsDescription(): ?string
    {
        return $this->ads_description;
    }

    public function setAdsDescription(string $ads_description): self
    {
        $this->ads_description = $ads_description;

        return $this;
    }

    public function getAdsPrice(): ?float
    {
        return $this->ads_price;
    }

    public function setAdsPrice(float $ads_price): self
    {
        $this->ads_price = $ads_price;

        return $this;
    }

    public function getAdsEmail(): ?string
    {
        return $this->ads_email;
    }

    public function setAdsEmail(string $ads_email): self
    {
        $this->ads_email = $ads_email;

        return $this;
    }

    public function getAdsSkype(): ?string
    {
        return $this->ads_skype;
    }

    public function setAdsSkype(?string $ads_skype): self
    {
        $this->ads_skype = $ads_skype;

        return $this;
    }

    public function getAdsLogin(): ?string
    {
        return $this->ads_login;
    }

    public function setAdsLogin(string $ads_login): self
    {
        $this->ads_login = $ads_login;

        return $this;
    }

    public function getAdsPassword(): ?string
    {
        return $this->ads_password;
    }

    public function setAdsPassword(string $ads_password): self
    {
        $this->ads_password = $ads_password;

        return $this;
    }

    public function getAdsCategoryId(): ?int
    {
        return $this->ads_category_id;
    }

    public function setAdsCategoryId(int $ads_category_id): self
    {
        $this->ads_category_id = $ads_category_id;

        return $this;
    }

    public function getAdsLocationId(): ?Locations
    {
        return $this->ads_location_id;
    }

    public function setAdsLocationId(?Locations $ads_location_id): self
    {
        $this->ads_location_id = $ads_location_id;

        return $this;
    }

    public function getAdsPhone(): ?string
    {
        return $this->ads_phone;
    }

    public function setAdsPhone(string $ads_phone): self
    {
        $this->ads_phone = $ads_phone;

        return $this;
    }

    public function getAdsCurrencyId(): ?Currency
    {
        return $this->ads_currency_id;
    }

    public function setAdsCurrencyId(?Currency $ads_currency_id): self
    {
        $this->ads_currency_id = $ads_currency_id;

        return $this;
    }
    

    public function getAdsViews(): ?int
    {
        return $this->ads_views;
    }

    public function setAdsViews(?int $ads_views): self
    {
        $this->ads_views = $ads_views;

        return $this;
    }


    public function getAdsDate(): ?\DateTimeInterface
    {
        return $this->ads_date;
    }

    public function setAdsDate(?\DateTimeInterface $ads_date): self
    {
        $this->ads_date = $ads_date;

        return $this;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getPictures(): Collection
    {
        return $this->Pictures;
    }

    public function addPicture(Picture $picture): self
    {
        if (!$this->Pictures->contains($picture)) {
            $this->Pictures[] = $picture;
            $picture->setPicAdsId($this);
        }

        return $this;
    }

    public function removePicture(Picture $picture): self
    {
        if ($this->Pictures->contains($picture)) {
            $this->Pictures->removeElement($picture);
            // set the owning side to null (unless already changed)
            if ($picture->getPicAdsId() === $this) {
                $picture->setPicAdsId(null);
            }
        }

        return $this;
    }
}

<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LocationsRepository")
 */
class Locations
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $loc_name;

    /**
     * @ORM\Column(type="integer")
     */
    private $loc_parent_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $lo_name_id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Advertisements", mappedBy="ads_location_id")
     */
    private $advertisemets;


    public function __construct()
    {
        $this->advertisements = new ArrayCollection();
        $this->advertisemets = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLocName(): ?string
    {
        return $this->loc_name;
    }

    public function setLocName(string $loc_name): self
    {
        $this->loc_name = $loc_name;

        return $this;
    }

    public function getLocParentId(): ?int
    {
        return $this->loc_parent_id;
    }

    public function setLocParentId(int $loc_parent_id): self
    {
        $this->loc_parent_id = $loc_parent_id;

        return $this;
    }

    public function getLocNameId(): ?int
    {
        return $this->lo_name_id;
    }

    public function setLocNameId(int $loc_name_id): self
    {
        $this->lo_name_id = $lo_name_id;

        return $this;
    }

    /**
     * @return Collection|Advertisements[]
     */
    public function getAdvertisemets(): Collection
    {
        return $this->advertisemets;
    }

    public function addAdvertisemet(Advertisements $advertisemet): self
    {
        if (!$this->advertisemets->contains($advertisemet)) {
            $this->advertisemets[] = $advertisemet;
            $advertisemet->setAdsLocationId($this);
        }

        return $this;
    }

    public function removeAdvertisemet(Advertisements $advertisemet): self
    {
        if ($this->advertisemets->contains($advertisemet)) {
            $this->advertisemets->removeElement($advertisemet);
            // set the owning side to null (unless already changed)
            if ($advertisemet->getAdsLocationId() === $this) {
                $advertisemet->setAdsLocationId(null);
            }
        }

        return $this;
    }

}

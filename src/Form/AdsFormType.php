<?php

namespace App\Form;

use App\Entity\Advertisements;
use App\Entity\Currency;
use App\Repository\CategoryRepository;
use App\Repository\CurrencyRepository;
use App\Repository\LocationsRepository;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


use Symfony\Component\Form\Extension\Core\Type\IntegerType;

use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class AdsFormType extends AbstractType
{
    private $curRepo;
    private $locationRepo;

    public function __construct(CurrencyRepository $curRepo, LocationsRepository $locationRepo)
    {
        $this->curRepo = $curRepo;
        $this->locationRepo = $locationRepo;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $allLocationObjects = $this->locationRepo->findAll();
        $LocationArrayNameObject['Any countries'] = 0;
        foreach ($allLocationObjects as $location) {
            if ($location->getLocParentId() == 0) {
                $LocationArrayNameObject[$location->getLocName()]['Any cities'] = $location->getId();
                foreach ($allLocationObjects as $subLocation) {
                    if ($subLocation->getLocParentId() == $location->getId()) {
                        $LocationArrayNameObject[$location->getLocName()][$subLocation->getLocName()] = $subLocation->getId();
                    }
                }
            }
        }

        $allCurObjects = $this->curRepo->findAll();
        $curArrayNameObject = [];
        foreach ($allCurObjects as $curObj) {
            $curArrayNameObject[$curObj->getCurName()] = $curObj;
        }

        $builder
            ->add('ads_title',NULL, ['label' => "Title"])
            ->add('ads_description',NULL, ['label' => "Description"])
            ->add('ads_category_id',CategoryType::class, ['label' => "Categoty"])
            ->add('ads_price',IntegerType::class, ['label' => "Price"])
            ->add('Pictures',PicturesType::class, ['label' => "Pictures"])
            //->add('ads_currency_id',CurrencyType::class, ['label' => "Currency"])
            ->add('ads_currency_id',ChoiceType::class, [
                'choices'  => $curArrayNameObject
            ])
            //->add('ads_location_id',LocationType::class, ['label' => "Locatinon"])
            ->add('ads_location_id',ChoiceType::class, [
                'choices'  => $LocationArrayNameObject
            ])
            ->add('ads_phone',NULL, ['label' => "Phone"])
            ->add('ads_email',NULL, ['label' => "Email"])
            ->add('ads_login',NULL, ['label' => "Login"])
            ->add('ads_password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'The password fields must match.',
                'options' => array('attr' => array('class' => 'password-field')),
                'required' => true,
                'first_options'  => array('label' => 'Password'),
                'second_options' => array('label' => 'Repeat Password'),
            ));
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Advertisements::class,
        ]);
    }
}

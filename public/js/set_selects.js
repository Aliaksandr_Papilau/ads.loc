$(document).ready(function() {
    var country_select = $('#country_select');
    var data_selected_country = country_select.attr('data-selected');
    country_select.val(data_selected_country);

    var city_select = $('#cities_select');
    var data_selected_city = city_select.attr('data-selected');
    city_select.val(data_selected_city);
});
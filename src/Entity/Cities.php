<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CitiesRepository")
 */
class Cities
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $city_name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Countries", inversedBy="cities")
     * @ORM\JoinColumn(nullable=false)
     */
    private $city_country_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $city_name_id;

    public function getId()
    {
        return $this->id;
    }

    public function getCityName(): ?string
    {
        return $this->city_name;
    }

    public function setCityName(string $city_name): self
    {
        $this->city_name = $city_name;

        return $this;
    }

    public function getCityCountryId(): ?Countries
    {
        return $this->city_country_id;
    }

    public function setCityCountryId(?Countries $city_country_id): self
    {
        $this->city_country_id = $city_country_id;

        return $this;
    }

    public function getCityNameId(): ?int
    {
        return $this->city_name_id;
    }

    public function setCityNameId(int $city_name_id): self
    {
        $this->city_name_id = $city_name_id;

        return $this;
    }
}

<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PictureRepository")
 */
class Picture
{
    public $file;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $pic_origin_name;

    /**
     * @ORM\Column(type="string", length=300)
     */
    private $pic_randomname;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Advertisements", inversedBy="Pictures")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pic_ads_id;




    public function __construct()
    {
        $this->pic_ads_id = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPicOriginName(): ?string
    {
        return $this->pic_origin_name;
    }

    public function setPicOriginName(string $pic_origin_name): self
    {
        $this->pic_origin_name = $pic_origin_name;

        return $this;
    }

    public function getPicRandomname(): ?string
    {
        return $this->pic_randomname;
    }

    public function setPicRandomname(string $pic_randomname): self
    {
        $this->pic_randomname = $pic_randomname;

        return $this;
    }

    public function getPicAdsId(): ?Advertisements
    {
        return $this->pic_ads_id;
    }

    public function setPicAdsId(?Advertisements $pic_ads_id): self
    {
        $this->pic_ads_id = $pic_ads_id;

        return $this;
    }





}

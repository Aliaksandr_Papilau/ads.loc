<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form;

use App\Entity\Category;
use App\Entity\Tag;
use App\Repository\CategoryRepository;
use App\Repository\LocationsRepository;
use App\Repository\TagRepository;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * This data transformer is used to translate the array of tags into a comma separated format
 * that can be displayed and managed by Bootstrap-tagsinput js plugin (and back on submit).
 *
 * See https://symfony.com/doc/current/form/data_transformers.html
 *
 * @author Yonel Ceruto <yonelceruto@gmail.com>
 * @author Jonathan Boyer <contact@grafikart.fr>
 */
class LocationTransformer implements DataTransformerInterface
{
    private $loc;

    public function __construct(LocationsRepository $loc)
    {
        $this->loc = $loc;
    }
    /**
     * {@inheritdoc}
     */
    public function transform($int)
    {
        return $int;
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($int)
    {
        return $this->loc->findOneBy(['id' => $int]);

    }
}

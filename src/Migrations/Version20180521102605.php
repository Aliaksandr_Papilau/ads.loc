<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180521102605 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE advertisements (id INT AUTO_INCREMENT NOT NULL, ads_location_id_id INT NOT NULL, ads_category_id_id INT NOT NULL, ads_title VARCHAR(100) NOT NULL, ads_description LONGTEXT NOT NULL, ads_price INT NOT NULL, ads_phone VARCHAR(20) NOT NULL, ads_email VARCHAR(50) NOT NULL, ads_skype VARCHAR(50) DEFAULT NULL, ads_login VARCHAR(50) NOT NULL, ads_password VARCHAR(50) NOT NULL, INDEX IDX_5C755F1E9B7343CC (ads_location_id_id), INDEX IDX_5C755F1E51C49A0 (ads_category_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE locations (id INT AUTO_INCREMENT NOT NULL, loc_name VARCHAR(50) NOT NULL, loc_parent_id INT NOT NULL, lo_name_id INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, cat_name VARCHAR(50) NOT NULL, cat_parent_cat INT NOT NULL, cat_name_id INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE advertisements ADD CONSTRAINT FK_5C755F1E9B7343CC FOREIGN KEY (ads_location_id_id) REFERENCES locations (id)');
        $this->addSql('ALTER TABLE advertisements ADD CONSTRAINT FK_5C755F1E51C49A0 FOREIGN KEY (ads_category_id_id) REFERENCES category (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE advertisements DROP FOREIGN KEY FK_5C755F1E9B7343CC');
        $this->addSql('ALTER TABLE advertisements DROP FOREIGN KEY FK_5C755F1E51C49A0');
        $this->addSql('DROP TABLE advertisements');
        $this->addSql('DROP TABLE locations');
        $this->addSql('DROP TABLE category');
    }
}

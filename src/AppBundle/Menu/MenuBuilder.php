<?php

// src/AppBundle/Menu/MenuBuilder.php.php
namespace App\AppBundle\Menu;

use App\Repository\CategoryRepository;
use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\Container;

class MenuBuilder
{
    private $factory;
    private $categoryRepo;

    /**
     * @param FactoryInterface $factory
     *
     * Add any other dependency you need
     */
    public function __construct(FactoryInterface $factory, CategoryRepository $repository)
    {
        $this->factory = $factory;
        $this->categoryRepo = $repository;
    }

    public function createTree1($category, $menu, $id = 0) {
        foreach ($category as $cat) {
            if ($cat->getCatParentCat() == $id) {
                $child = $menu->addChild($cat->getCatName());
                $child->setAttribute('class','parent');
                $this->createTree1($category, $child, $cat->getId());
            }
        }
    }

    public function createCategoriesMenu(Array $options)
    {
        $menu = $this->factory->createItem('menu');
        $category = $this->categoryRepo->findAll();
        $this->createTree1($category, $menu);
        return $menu;
    }

    public function createSecondMenu(array $options)
    {
        $menu = $this->factory->createItem('root');

        $menu->addChild('Second');
        // ... add more children

        return $menu;
    }
}
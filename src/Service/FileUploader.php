<?php
namespace App\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{

    private function getFiveHashes(string $fileName, string $homedirsLocation) : string
    {

        do {
            $fileName = sha1($fileName) . sha1((string)microtime(true)) . sha1((string)mt_rand(1000000000, 2000000000)) . sha1(strrev($fileName)) . sha1((string)(microtime(true) * mt_rand(1000000000, 2000000000)));
        }
        while (is_file($homedirsLocation . $fileName));
        return $fileName;
    }

    public function upload(string $path, UploadedFile $file) : string
    {
        $fileName = $file->getClientOriginalName();
        //todo add extention to the filename
        $fileName = $this->getFiveHashes($path, $fileName);

        $file->move($path, $fileName);

        return $fileName;
    }
}
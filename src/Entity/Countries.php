<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CountriesRepository")
 */
class Countries
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $c_name;

    /**
     * @ORM\Column(type="integer")
     */
    private $c_name_id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cities", mappedBy="city_country_id")
     */
    private $cities;

    public function __construct()
    {
        $this->cities = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCName(): ?string
    {
        return $this->c_name;
    }

    public function setCName(string $c_name): self
    {
        $this->c_name = $c_name;

        return $this;
    }

    public function getCNameId(): ?int
    {
        return $this->c_name_id;
    }

    public function setCNameId(int $c_name_id): self
    {
        $this->c_name_id = $c_name_id;

        return $this;
    }

    /**
     * @return Collection|Cities[]
     */
    public function getCities(): Collection
    {
        return $this->cities;
    }

    public function addCity(Cities $city): self
    {
        if (!$this->cities->contains($city)) {
            $this->cities[] = $city;
            $city->setCityCountryId($this);
        }

        return $this;
    }

    public function removeCity(Cities $city): self
    {
        if ($this->cities->contains($city)) {
            $this->cities->removeElement($city);
            // set the owning side to null (unless already changed)
            if ($city->getCityCountryId() === $this) {
                $city->setCityCountryId(null);
            }
        }

        return $this;
    }
}

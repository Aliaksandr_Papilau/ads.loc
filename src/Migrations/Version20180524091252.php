<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180524091252 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE picture ADD pic_ads_id_id INT NOT NULL');
        $this->addSql('ALTER TABLE picture ADD CONSTRAINT FK_16DB4F897D5631B FOREIGN KEY (pic_ads_id_id) REFERENCES advertisements (id)');
        $this->addSql('CREATE INDEX IDX_16DB4F897D5631B ON picture (pic_ads_id_id)');
        $this->addSql('ALTER TABLE advertisements DROP FOREIGN KEY FK_5C755F1EEE45BDBF');
        $this->addSql('DROP INDEX IDX_5C755F1EEE45BDBF ON advertisements');
        $this->addSql('ALTER TABLE advertisements DROP picture_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE advertisements ADD picture_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE advertisements ADD CONSTRAINT FK_5C755F1EEE45BDBF FOREIGN KEY (picture_id) REFERENCES picture (id)');
        $this->addSql('CREATE INDEX IDX_5C755F1EEE45BDBF ON advertisements (picture_id)');
        $this->addSql('ALTER TABLE picture DROP FOREIGN KEY FK_16DB4F897D5631B');
        $this->addSql('DROP INDEX IDX_16DB4F897D5631B ON picture');
        $this->addSql('ALTER TABLE picture DROP pic_ads_id_id');
    }
}

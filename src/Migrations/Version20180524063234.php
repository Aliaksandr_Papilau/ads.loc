<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180524063234 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE picture (id INT AUTO_INCREMENT NOT NULL, pic_origin_name VARCHAR(200) NOT NULL, pic_randomname VARCHAR(300) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE currency (id INT AUTO_INCREMENT NOT NULL, cur_name VARCHAR(5) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE advertisements ADD ads_currency_id_id INT NOT NULL, ADD picture_id INT DEFAULT NULL, ADD ads_views INT DEFAULT NULL, ADD ads_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE advertisements ADD CONSTRAINT FK_5C755F1EBACD048F FOREIGN KEY (ads_currency_id_id) REFERENCES currency (id)');
        $this->addSql('ALTER TABLE advertisements ADD CONSTRAINT FK_5C755F1EEE45BDBF FOREIGN KEY (picture_id) REFERENCES picture (id)');
        $this->addSql('CREATE INDEX IDX_5C755F1EBACD048F ON advertisements (ads_currency_id_id)');
        $this->addSql('CREATE INDEX IDX_5C755F1EEE45BDBF ON advertisements (picture_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE advertisements DROP FOREIGN KEY FK_5C755F1EEE45BDBF');
        $this->addSql('ALTER TABLE advertisements DROP FOREIGN KEY FK_5C755F1EBACD048F');
        $this->addSql('DROP TABLE picture');
        $this->addSql('DROP TABLE currency');
        $this->addSql('DROP INDEX IDX_5C755F1EBACD048F ON advertisements');
        $this->addSql('DROP INDEX IDX_5C755F1EEE45BDBF ON advertisements');
        $this->addSql('ALTER TABLE advertisements DROP ads_currency_id_id, DROP picture_id, DROP ads_views, DROP ads_date');
    }
}

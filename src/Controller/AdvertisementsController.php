<?php

namespace App\Controller;

use App\AppBundle\Menu\MenuBuilder;
use App\Entity\Advertisements;
use App\Entity\Ads;
use App\Entity\Category;
use App\Entity\Locations;
use App\Form\AdsFormType;
use App\Form\AdsFormTypeNew;
use App\Service\FileUploader;
use App\Service\MessageGenerator;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Request;


use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\Response;

use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
use Pagerfanta\View\DefaultView;
use Pagerfanta\Adapter\DoctrineORMAdapter;



use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Forms;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\HttpFoundation\File\UploadedFile;

//use App\AppBundle\Menu\MenuBuilder;

class AdvertisementsController extends Controller
{
    /**
     * main page
     *
     * @Route("/", defaults={"page": "1"}, name="advertisements")
     *
     */
    public function index($page, SessionInterface $session)
    {

        $session->remove('chainCategories');
        $session->remove('chainLocations');
        $session->remove('cityId');
        $session->remove('countryId');
        $session->remove('parentId');

        $countries = $this->getDoctrine()
            ->getRepository('App\Entity\Locations')
            ->findBy(['loc_parent_id' => 0]);

        $session->set('countries', $countries);

        $currentCategories = $this->getDoctrine()
            ->getRepository('App\Entity\Category')
            ->findBy(['cat_parent_cat' => 0]);

        $advertisments = $this->getDoctrine()
            ->getRepository('App\Entity\Advertisements')
            ->findAll();

        $adapter = new ArrayAdapter($advertisments);

        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(7);
        $pagerfanta->setCurrentPage($page);


        return $this->render('advertisements/index.html.twig', [
            'currentCategories' => $currentCategories,
            'pagerfanta' => $pagerfanta,
            'countries' => $countries,
            'cities' => [],
            'countryId' => 0,
            'cityId' => 0,
            'bradcrumpsData' => [],
        ]);
    }

    /**
     * page shows ads according to given category, country and city
     *
     * @Route("/filter/{parentId}", defaults={"page": "1"}, name="filter")
     * @Route("/paginate/{page}", requirements={"page": "[1-9]\d*"}, name="filter_paginated")
     */
    public function filter($page, $parentId = NULL, SessionInterface $session)
    {
        if ($session->has('parentId') && ($parentId === NULL)) {
            $parentId = $session->get('parentId');
        } elseif ($parentId === NULL){
            $parentId = 0;
            $session->set('parentId', $parentId);
        } else {
            $session->set('parentId', $parentId);
        }

        //setting default values
        $cityId = 0;
        $countryId = 0;
        $cities = [];
        $countries = $session->get('countries');
        $currentCategories = $this->getDoctrine()
            ->getRepository('App\Entity\Category')
            ->findBy(['cat_parent_cat' => $parentId]);

        //is used for finding all hierarchy of categories below given id
        $chainCategories = $this->getDoctrine()
            ->getRepository('App\Entity\Category')
            ->getCategoriesChain($parentId);
        $bradcrumpsData = $this->getDoctrine()
            ->getRepository('App\Entity\Category')
            ->getBreadCrumps($parentId);

        //we are saving current category hierarchy not to lose it when we change location using AJAX

        $session->set('chainCategories', $chainCategories);
        $session->set('parentId', $parentId);

        if ($session->has('cityId')) {
            $advertisments = $this->getDoctrine()
                ->getRepository('App\Entity\Advertisements')
                ->findBy(['ads_category_id' => $chainCategories, 'ads_location_id' => $session->get('cityId')]);
                $countryId = $session->get('countryId');
                $cityId = $session->get('cityId');
                $cities = $session->get('cities');
        } elseif ($session->has('countryId')) {
            $countryId = $session->get('countryId');

            $cities = $this->getDoctrine()
                ->getRepository('App\Entity\Locations')
                ->findBy(['loc_parent_id' => $countryId]);

            $chainLocations = $this->getDoctrine()
                ->getRepository('App\Entity\Locations')
                ->getLocationsChain($countryId);
            $advertisments = $this->getDoctrine()
                ->getRepository('App\Entity\Advertisements')
                ->findBy(['ads_category_id' => $chainCategories, 'ads_location_id' => $chainLocations]);
        } else {
            $advertisments = $this->getDoctrine()
                ->getRepository('App\Entity\Advertisements')
                ->findBy(['ads_category_id' => $chainCategories]);
        }

        $adapter = new ArrayAdapter($advertisments);

        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(7);
        $pagerfanta->setCurrentPage($page);

        return $this->render('advertisements/index.html.twig', [
            'bradcrumpsData' => $bradcrumpsData,
            'currentCategories' => $currentCategories,
            'pagerfanta' => $pagerfanta,
            'countries' => $countries,
            'cities' => $cities,
            'cityId' => $cityId,
            'countryId' => $countryId,
        ]);
    }

    /**
     * @Route("ajax_get_countries", name="ajax_get_countries")
     */
    public function ajax_get_countries(Request $request, SessionInterface $session)
    {
        $responce['cities'] = [];

        $countryId = $request->request->get('countryId');
        $session->set('countryId', $countryId);

        if ($session->has('chainCategories')) {
            $chainCategories = $session->get('chainCategories');
        }


        if ($countryId == 0) {
            //show all
            if (isset($chainCategories)) {
                $advertisements = $this->getDoctrine()
                    ->getRepository('App\Entity\Advertisements')
                    ->findBy(['ads_category_id' => $chainCategories]);
            } else {
                $advertisements = $this->getDoctrine()
                    ->getRepository('App\Entity\Advertisements')
                    ->findAll();
            }
        } elseif ($countryId > 0) {
            //show by country chain


            $responce['cities'] = $this->getDoctrine()
                ->getRepository('App\Entity\Locations')
                ->findBy(['loc_parent_id' => $countryId]);
            $session->set('cities', $responce['cities']);

            $chainLocations = $this->getDoctrine()
                ->getRepository('App\Entity\Locations')
                ->getLocationsChain($countryId);

            $session->set('chainLocations', $chainLocations);

            if (isset($chainCategories)) {
                $advertisements = $this->getDoctrine()
                    ->getRepository('App\Entity\Advertisements')
                    ->findBy(['ads_category_id' => $chainCategories, 'ads_location_id' => $chainLocations]);
            } else {
                $advertisements = $this->getDoctrine()
                    ->getRepository('App\Entity\Advertisements')
                    ->findBy(['ads_location_id' => $chainLocations]);
            }
        }


        $adapter = new ArrayAdapter($advertisements);

        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(7);
        $pagerfanta->setCurrentPage(1);


        $responce['paginationHtml'] = $this->render('advertisements/pagination.html.twig', [
            'pagerfanta' => $pagerfanta,
        ]);

        $responce['htmlAds'] = $this->render('advertisements/ads_prev.html.twig', [
            'pagerfanta' => $pagerfanta,
        ]);

        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return NULL;
        });

        $serializer = new Serializer(array($normalizer), array($encoder));

        return new Response($serializer->serialize($responce, 'json'));

    }
    /**
     * @Route("ajax_get_cities", name="ajax_get_cities")
     */
    public function ajax_get_cities(Request $request, SessionInterface $session)
    {
        $cityId = $request->request->get('city_id');
        $session->set('cityId', $cityId);
        if ($session->has('chainCategories')) {
            $chainCategories = $session->get('chainCategories');
        } else {
            $chainCategories = NULL;
        }

        if ($session->has('chainLocations')) {
            $chainLocations = $session->get('chainLocations');
        }

        if ($cityId == 0) {
            if (isset($chainCategories) && isset($chainLocations)) {
                $advertisements = $this->getDoctrine()
                    ->getRepository('App\Entity\Advertisements')
                    ->findBy(['ads_location_id' => $chainLocations, 'ads_category_id' => $chainCategories]);
            } elseif (is_null($chainCategories) && is_null($chainLocations)) {
                $advertisements = $this->getDoctrine()
                    ->getRepository('App\Entity\Advertisements')
                    ->findAll();
            } elseif (is_null($chainCategories) && isset($chainLocations)) {
                $advertisements = $this->getDoctrine()
                    ->getRepository('App\Entity\Advertisements')
                    ->findBy(['ads_location_id' => $chainLocations]);
            } elseif (isset($chainCategories) && is_null($chainLocations)) {
                $advertisements = $this->getDoctrine()
                    ->getRepository('App\Entity\Advertisements')
                    ->findBy(['ads_category_id' => $chainCategories]);
            }
        } else {
            if (isset($chainCategories)) {
                $advertisements = $this->getDoctrine()
                    ->getRepository('App\Entity\Advertisements')
                    ->findBy(['ads_location_id' => $cityId, 'ads_category_id' => $chainCategories]);
            } else {
                $advertisements = $this->getDoctrine()
                    ->getRepository('App\Entity\Advertisements')
                    ->findBy(['ads_location_id' => $cityId]);
            }
        }



        $adapter = new ArrayAdapter($advertisements);

        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(7);
        $pagerfanta->setCurrentPage(1);

        $responce['paginationHtml'] = $this->render('advertisements/pagination.html.twig', [
            'pagerfanta' => $pagerfanta,
        ]);


        $responce['htmlAds'] = $this->render('advertisements/ads_prev.html.twig', [
            'pagerfanta' => $pagerfanta,
        ]);

        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return NULL;
        });

        $serializer = new Serializer(array($normalizer), array($encoder));

        return new Response($serializer->serialize($responce, 'json'));
    }

    /**
     * @Route("ads/{id}", name="showads")
     */
    public function showAds(Advertisements $ads)
    {
        $countries = $this->getDoctrine()
            ->getRepository('App\Entity\Locations')
            ->findBy(['loc_parent_id' => 0]);
        $currentCategories = $this->getDoctrine()
            ->getRepository('App\Entity\Category')
            ->findBy(['cat_parent_cat' => 0]);
        $entityManager = $this->getDoctrine()->getManager();
        $views = $ads->getAdsViews();
        $ads->setAdsViews(++$views);
        $entityManager->persist($ads);
        $entityManager->flush();

        return $this->render('advertisements/ads.html.twig', [
            'ads' => $ads,
            'currentCategories' => $currentCategories,
            'countries' => $countries,
            'cities' => [],
            'countryId' => 0,
            'cityId' => 0,
        ]);
    }

    /**
     * @Route("/publish", name="publish")
     */

    public function publishAds(Request $request, FileUploader $uploader) : Response
    {

        $countries = $this->getDoctrine()
            ->getRepository('App\Entity\Locations')
            ->findBy(['loc_parent_id' => 0]);
        $currentCategories = $this->getDoctrine()
            ->getRepository('App\Entity\Category')
            ->findBy(['cat_parent_cat' => 0]);

        // 1) build the form
        $ads = new Advertisements();
        $form = $this->createForm(AdsFormType::class, $ads)
            ->add('saveAndCreateNew', SubmitType::class);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $pictures = $ads->getPictures();
            foreach ($pictures as &$pic) {
                $pic->file;
                $randName = $uploader->upload($this->getParameter('pictures_directory'), $pic->file);
                $pic->setPicRandomname($randName);
                $pic->setPicOriginName( $pic->file->getClientOriginalName());
            }
            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($ads);
            $entityManager->flush();

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user

            return $this->redirectToRoute('advertisements');
        }

        return $this->render('advertisements/publish.html.twig', [
            'form' => $form->createView(),
            'countries' => $countries,
            'currentCategories' => $currentCategories,
            'cities' => [],
            'countryId' => 0,
            'cityId' => 0,
        ]);
    }

}

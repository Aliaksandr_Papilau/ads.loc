<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180521104944 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE countries (id INT AUTO_INCREMENT NOT NULL, c_name VARCHAR(50) NOT NULL, c_name_id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cities (id INT AUTO_INCREMENT NOT NULL, city_country_id_id INT NOT NULL, city_name VARCHAR(50) NOT NULL, city_name_id INT NOT NULL, INDEX IDX_D95DB16BA53389A5 (city_country_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE cities ADD CONSTRAINT FK_D95DB16BA53389A5 FOREIGN KEY (city_country_id_id) REFERENCES countries (id)');
        $this->addSql('ALTER TABLE advertisements DROP FOREIGN KEY FK_5C755F1E51C49A0');
        $this->addSql('DROP INDEX IDX_5C755F1E51C49A0 ON advertisements');
        $this->addSql('ALTER TABLE advertisements CHANGE ads_title ads_title VARCHAR(255) NOT NULL, CHANGE ads_price ads_price DOUBLE PRECISION NOT NULL, CHANGE ads_login ads_login VARCHAR(30) NOT NULL, CHANGE ads_password ads_password VARCHAR(30) NOT NULL, CHANGE ads_category_id_id ads_category_id INT NOT NULL');
        $this->addSql('ALTER TABLE locations CHANGE loc_name loc_name VARCHAR(40) NOT NULL, CHANGE lo_name_id lo_name_id INT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cities DROP FOREIGN KEY FK_D95DB16BA53389A5');
        $this->addSql('DROP TABLE countries');
        $this->addSql('DROP TABLE cities');
        $this->addSql('ALTER TABLE advertisements CHANGE ads_title ads_title VARCHAR(100) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE ads_price ads_price INT NOT NULL, CHANGE ads_login ads_login VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE ads_password ads_password VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE ads_category_id ads_category_id_id INT NOT NULL');
        $this->addSql('ALTER TABLE advertisements ADD CONSTRAINT FK_5C755F1E51C49A0 FOREIGN KEY (ads_category_id_id) REFERENCES category (id)');
        $this->addSql('CREATE INDEX IDX_5C755F1E51C49A0 ON advertisements (ads_category_id_id)');
        $this->addSql('ALTER TABLE locations CHANGE loc_name loc_name VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE lo_name_id lo_name_id INT DEFAULT NULL');
    }
}

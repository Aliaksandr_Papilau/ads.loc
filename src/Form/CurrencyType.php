<?php
namespace App\Form;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use App\Repository\CurrencyRepository;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;

class CurrencyType extends AbstractType
{
    public $cur;
    public function __construct(CurrencyRepository $cur)
    {
        $this->cur = $cur;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // The Tag collection must be transformed into a comma separated string.
            // We could create a custom transformer to do Collection <-> string in one step,
            // but here we're doing the transformation in two steps (Collection <-> array <-> string)
            // and reuse the existing CollectionToArrayTransformer.
            ->addModelTransformer(new CurrencyTransformer($this->cur))
        ;
    }

    public function getParent()
    {
        return IntegerType::class;
    }
}

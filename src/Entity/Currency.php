<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CurrencyRepository")
 */
class Currency
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $cur_name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Advertisements", mappedBy="ads_currency_id")
     */
    private $adverticements;

    public function __construct()
    {
        $this->adverticements = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCurName(): ?string
    {
        return $this->cur_name;
    }

    public function setCurName(string $cur_name): self
    {
        $this->cur_name = $cur_name;

        return $this;
    }

    /**
     * @return Collection|Advertisements[]
     */
    public function getAdverticements(): Collection
    {
        return $this->adverticements;
    }

    public function addAdverticement(Advertisements $adverticement): self
    {
        if (!$this->adverticements->contains($adverticement)) {
            $this->adverticements[] = $adverticement;
            $adverticement->setAdsCurrencyId($this);
        }

        return $this;
    }

    public function removeAdverticement(Advertisements $adverticement): self
    {
        if ($this->adverticements->contains($adverticement)) {
            $this->adverticements->removeElement($adverticement);
            // set the owning side to null (unless already changed)
            if ($adverticement->getAdsCurrencyId() === $this) {
                $adverticement->setAdsCurrencyId(null);
            }
        }

        return $this;
    }
}
